//
//  Box.swift
//  Konteynr
//
//  Created by Anıl Kılıç on 11.10.2016.
//  Copyright © 2016 Anıl Kılıç. All rights reserved.
//

import UIKit
import SceneKit

class Box: SCNNode {
    
    convenience init(w: CGFloat, h: CGFloat, l: CGFloat, x: CGFloat, y: CGFloat, z: CGFloat) {
        self.init()
        
        // MARK: Box Material
        let boxMat = SCNMaterial()
        boxMat.diffuse.contents = getRandomColor()
        
        // MARK: Box Geometry
        let boxGeo = SCNBox(width: w, height: h, length: l, chamferRadius: 0)
        boxGeo.firstMaterial = boxMat
        
        // MARK: Box Node
        let box = SCNNode(geometry: boxGeo)
        box.position = SCNVector3(x,y,z)
        // genişliği ve derinliği 3 ise, merkez noktasını, -1.5 kaydırarak 0'a getiriyorum.
        box.pivot = SCNMatrix4MakeTranslation(-( Float(w / 2)), 0, -( Float(l / 2)))
        box.physicsBody = SCNPhysicsBody.static()
        
        self.addChildNode(box)
    }
    
    func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48()), randomGreen:CGFloat = CGFloat(drand48()), randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
}
