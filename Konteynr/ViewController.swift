//
//  ViewController.swift
//  Konteynr
//
//  Created by Anıl Kılıç on 11.10.2016.
//  Copyright © 2016 Anıl Kılıç. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import CoreMotion

class ViewController: UIViewController, SCNSceneRendererDelegate, addBoxDelegate {
    
    // let -> Constant
    let scnView = SCNView()
    let scene = SCNScene()
    
    // var -> Variable
    var taban: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()

        setupLightsAndCamera()
        setupEnvironment()
        
        // Ortadaki kırmızı nokta, merkezi x: 0, y: 0, z: 0
        setupPivotPoint()
    }
    
    func setupViews() {
        navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(presentAddView)), animated: true)
        
        scnView.translatesAutoresizingMaskIntoConstraints = false
        scnView.backgroundColor = UIColor.blue
        
        scnView.scene = scene
        scnView.allowsCameraControl = true
        scnView.showsStatistics = true
        scnView.delegate = self
        
        view.addSubview(scnView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v1]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": scnView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[v1]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": scnView]))
    }

    func setupLightsAndCamera() {
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 10, z: 35)
        
        let lightNodeDirnl = SCNNode()
        lightNodeDirnl.light = SCNLight()
        lightNodeDirnl.light!.type = .directional
        lightNodeDirnl.position = SCNVector3(x: 0, y: 10, z: 5)
        
        let ambiLightNode = SCNNode()
        ambiLightNode.light = SCNLight()
        ambiLightNode.light?.type = .ambient
        ambiLightNode.light?.color = UIColor.gray
        
        let center = SCNNode()
        lightNodeDirnl.constraints = [SCNLookAtConstraint(target: center)]
        cameraNode.constraints = [SCNLookAtConstraint(target: center)]
        
        scene.rootNode.addChildNode(cameraNode)
        scene.rootNode.addChildNode(lightNodeDirnl)
        scene.rootNode.addChildNode(ambiLightNode)
    }
    
    func setupEnvironment() {
        let floorGeo = SCNFloor()
        floorGeo.firstMaterial?.diffuse.contents = UIColor(red: 0.3, green: 0.5, blue: 0.3, alpha: 1.0)
        let floorNode = SCNNode(geometry: floorGeo)
        
        floorNode.physicsBody = SCNPhysicsBody(type: SCNPhysicsBodyType.static, shape: SCNPhysicsShape(geometry: floorGeo, options: nil))
        scene.rootNode.addChildNode(floorNode)
    }
    
    func setupPivotPoint() {
        let pointGeo = SCNSphere(radius: 1)
        let pointMat = SCNMaterial()
        pointMat.diffuse.contents = UIColor.red
        pointGeo.firstMaterial = pointMat
        
        let point = SCNNode(geometry: pointGeo)
        point.position = SCNVector3(0,0,0)
        scene.rootNode.addChildNode(point)
        
        point.physicsBody = SCNPhysicsBody.static()
    }
    
    func presentAddView() {
        let dvc = SecondViewController()
        dvc.delegate = self
        navigationController?.pushViewController(dvc, animated: true)
    }
    
    func handleAddBox(w: CGFloat, h: CGFloat, l: CGFloat, c: Int) {
        var setY : CGFloat = h / 2
        for _ in 0...(c - 1) {
            scene.rootNode.addChildNode(Box(w: w, h: h, l: l, x: taban, y: setY, z: 0))
            setY = setY + h
        }
        taban = taban + w
    }
}

