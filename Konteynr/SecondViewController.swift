//
//  SecondViewController.swift
//  Konteynr
//
//  Created by Anıl Kılıç on 11.10.2016.
//  Copyright © 2016 Anıl Kılıç. All rights reserved.
//

import UIKit
import SceneKit

protocol addBoxDelegate {
    func handleAddBox(w: CGFloat, h: CGFloat, l: CGFloat, c: Int)
}

class SecondViewController: UIViewController, SCNSceneRendererDelegate {
    
    var delegate : addBoxDelegate? = nil

    let widthField = UITextField()
    let heightField = UITextField()
    let lenghtField = UITextField()
    let countField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        view.backgroundColor = UIColor.white
        navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddBox)), animated: true)

        let viewsArray = [widthField,heightField,lenghtField,countField]
        
        widthField.placeholder = "width"
        heightField.placeholder = "height"
        lenghtField.placeholder = "lenght"
        countField.placeholder = "count"
        
        for item in viewsArray {
            item.translatesAutoresizingMaskIntoConstraints = false
            item.borderStyle = .roundedRect
            item.keyboardType = .numberPad
            self.view.addSubview(item)
        }
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[v1]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": widthField]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[v1]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": heightField]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[v1]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": lenghtField]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[v1]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": countField]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-80-[v1]-[v2]-[v3]-[v4]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v1": widthField, "v2": heightField, "v3": lenghtField, "v4": countField]))
    }
    
    func handleAddBox() {
        delegate?.handleAddBox(w: CGFloat(Float(widthField.text!)!), h: CGFloat(Float(heightField.text!)!) , l: CGFloat(Float(lenghtField.text!)!) , c: Int(countField.text!)!)
        _ = navigationController?.popViewController(animated: true)
    }

}
